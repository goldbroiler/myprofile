import {assert} from 'assertthat';

describe('entries', () => {
    it('should contain a references.json and not undefined', async() => {
        const refs =  require('./references.json');
        assert.that(refs).is.not.equalTo('undefined')
    })
})