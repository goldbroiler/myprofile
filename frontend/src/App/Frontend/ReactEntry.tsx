import { FunctionComponent, ReactElement } from 'react';
import Card from 'react-bootstrap/Card';
import ListGroup  from 'react-bootstrap/ListGroup';



const ReactEntry: FunctionComponent= (): ReactElement => {


    return (
        <Card className="alert alert-success w-100 p-0  react-infos">
    
            <Card.Body>
                <Card.Title>
                    <h2>Mein Profil - als React Anwendung</h2></Card.Title>
                <Card.Text>
                
                Da ich mit React erst begonnen habe, habe ich mich entschlossen, dieses Profil als React Anwendung zu bauen und mit in den Referenzen aufzuführen. Zudem biete ich hier den Code-Download an, da auch das ggfs. wichtig sein könnte.<br></br><a href="https://bitbucket.org/goldbroiler/myprofile/src/master/" target="_blank">Code Frontend auf Bitbucket</a><br></br>
                Die Referenzen kommen aus einer MongoDB und werden via Node/Express REST API ausgeliefert.

                   
                    <Card className="innercard alert alert-warning p-0">
                        <Card.Body>
                            <Card.Title>Arbeiten</Card.Title>
                            <ListGroup className="work">
                                <ListGroup.Item>React Anwendung (Function Components mit useState/UseEffect Hooks)</ListGroup.Item>
                                <ListGroup.Item>Umsetzung mit TypeScript, React-Bootstap, React-Masonry-CSS</ListGroup.Item>
                                <ListGroup.Item>Node / Express Backend (REST API) für die JSON Referenzen | <a href="https://bitbucket.org/goldbroiler/node-mini-rest-api/src/master/" target="_blank">Backend auf Bitbucket</a></ListGroup.Item>
                                <ListGroup.Item>MongoDB für die Speicherung der Referenzen</ListGroup.Item>
                                <ListGroup.Item>Firebase Cloud Functions für das Backend</ListGroup.Item>
                                <ListGroup.Item>Bitbucket Pipelines</ListGroup.Item>
                            </ListGroup>
                        </Card.Body>
                    </Card>    

                    <Card className="innercard alert alert-warning p-0">
                        <Card.Body>
                            <Card.Title>Skills</Card.Title>
                            <Card.Text>
                                    React, React-Bootstrap, TypeScript, MongoDB, Firebase, REST API, Express
                            </Card.Text>
                        </Card.Body>
                    </Card>    
                   

                </Card.Text>
          
            </Card.Body>
        </Card>
    )
}     

export {ReactEntry}  