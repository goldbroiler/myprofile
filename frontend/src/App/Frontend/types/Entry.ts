/**wordpress entry type */

type Rendered = {rendered: string};

type RenderedContent = {rendered: HTMLParagraphElement, protected?: boolean}

interface PostMeta {
    street: string;
    zip: string | number;
    city: string;
    teaser: string;
    startdate: string;
    enddate: string;
    image1: number;
    image2: number;
    image3: number;
    image4: number;
    _thumbnail_id?: number;
}

interface Entry {
    id: number;
    date: any;
    date_gmt: any; 
    guid: Rendered,
    modified: any;
    modified_gmt: any;
    slug: string;
    status: string;
    type: string;
    link:URL;
    title: Rendered;
    content: RenderedContent;
    excerpt: RenderedContent;
    author: number;
    featured_media: number;
    comment_status: string;
    ping_status: string;
    sticky: boolean;
    template?: any;
    format: string;
    meta:PostMeta;
    categories: number[];
    tags: number[];
    acf: [],
    _links: any;
    _embedded: any;
}

export {Entry}
       