import ReactDOM from 'react-dom';

import { App } from './App/App';

const domTarget = document.getElementById('app');

ReactDOM.render(<App />, domTarget);